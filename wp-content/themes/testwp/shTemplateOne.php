<?php
/*
Template name: shTemplateOne
*/

get_header();
?>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

        <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a>
        </h1>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <nav class="collapse navbar-collapse" id="navbarCollapse">

            <nav id="site-navigation" class="main-navigation">
                <style>
                    .page_item {
                        margin-left: 10px;
                    }
                </style>
                <button class="menu-toggle" aria-controls="primary-menu"
                        aria-expanded="false"><?php esc_html_e('Primary Menu', 'testwp'); ?></button>
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-1',
                        'menu_id' => 'primary-menu',
                    )
                );
                ?>
            </nav>
        </nav>
    </nav>

    <main role="main">

        <div class="wrap">

            <div class="container">
                <h1>Test page</h1>
                <h3>template 2</h3>
            </div>


        </div>

<?php
get_sidebar();
get_footer();